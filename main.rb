#!/usr/bin/env ruby

# frozen_string_literal: true

require 'optparse'
require 'socket'

class Cli
  attr_reader :host, :user, :port, :password, :dbname

  def initialize(password:, configuration:)
    configuration, @host, @port, @user, @dbname = configuration.match(/host=(\w+) port=(\d+) user=(\w+) dbname=(\w+)/).to_a
    @dbname = 'gitlabhq_dblab' if dbname == 'DBNAME'
    @password = password
  end

  def start
    ssh_pid = fork do
      exec("ssh -NTML #{port}:#{host}:#{port} gitlab-joe-poc.postgres.ai")
    end
    Process.detach(ssh_pid)

    psql_pid = fork do
      wait_for_connections
      exec("psql 'host=#{host} port=#{port} user=#{user} dbname=#{dbname} password=#{password}'")
    end

    Process.wait(psql_pid)
    Process.kill("HUP", ssh_pid)
  end

  def wait_for_connections
    until port_open?
      sleep 0.02
    end
  end

  def port_open?
    TCPSocket.new(host, port)
    true
  rescue Errno::ECONNREFUSED
    false
  end
end

if $PROGRAM_NAME == __FILE__
  options = {}

  OptionParser.new do |opts|
    opts.on("-p", "--password STRING", String, "PG password") do |value|
      options[:password] = value
    end

    opts.on("-c", "--configuration STRING", String, "psql configuration string") do |value|
      options[:configuration] = value
    end

    opts.on("-h", "--help", "Prints this help") do
      puts opts
      exit
    end
  end.parse!

  Cli.new(**options).start
end
